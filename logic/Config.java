package logic;

public class Config {

	public static int PARENTS = 20;
	public static int PARENT_GENERATIONS = 5;
	public static int MAX_GENERATIONS = 50000;
	public static int PROBABILITY = 22;
	public static int MIX_PROBABILITY = 60;
	/**
	 * Maximally how many genes are being mutated during mutation <br>(Should be less than half the Garden perimeter - else all of them can be mutated)
	 */
	public static int MUTATION_COUNT = 5;
	public static int X = 10;
	public static int Y = 12;
	public static int ROULETTE = 20;
	
}
