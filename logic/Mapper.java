package logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;


public class Mapper {

	private int xlength = 0;
	private int ylength = 0;
	private LinkedList<Gene> stones = new LinkedList<Gene>();
	
	public Mapper() {
		
	}
	
	@SuppressWarnings("resource")
	public void readGarden(File file) throws IOException {
	
		BufferedReader buffer = new BufferedReader(new FileReader(file));
		String line = "";
		this.xlength = 0;
		
		while ((line = buffer.readLine()) != null) {
			if (ylength==0) {
				this.ylength = line.length();
			}
			this.xlength++;
			for (int i = 0; i<line.length(); i++) {
				if(line.charAt(i)=='K') {
					stones.add(new Gene(xlength - 1, i, 's'));
				}
			}
		}
	}
	
	public int getXlength() {
		return xlength;
	} 
	
	public int getYlength() {
		return ylength;
	}
	
	public LinkedList<Gene> getStones() {
		return stones;
	}
}
