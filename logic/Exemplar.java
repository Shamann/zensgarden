package logic;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Exemplar {

	private LinkedList<Gene> stones = new LinkedList<Gene>();
	private List<Gene> chromosome;
	private boolean goal = false;
	private int fitnes = -1;
	private Garden garden;
	private static int maxGenes;

	private int generation = 1;

	public Exemplar(int x, int y, int generation) {
		chromosome = new ArrayList<Gene>(x + y);
		garden = new Garden(x, y);
		maxGenes = x + y;
		this.generation = generation;
	}
	
	public Exemplar(Exemplar exemplar) {
		stones = exemplar.getStones();
		chromosome = exemplar.getChromosome();
		maxGenes = Exemplar.getMaxGenes();
		garden = new Garden(exemplar.getGarden().getX(), exemplar.getGarden().getY());
		generation = exemplar.getGeneration()+1;
	}

	public String toString(){
		String output = "";
		for(Gene g : chromosome) {
			output += g.toString() + " ";
		}
		output += "fitnes: " + fitnes;
		return output;
	}
	
	public LinkedList<Gene> getStones() {
		return stones;
	}

	/**
	 * adds new Gene/stone position into exemplar.
	 * 
	 * @param x
	 *            - vertical position
	 * @param y
	 *            - horizontal position
	 */
	public void addStone(int x, int y) {
		stones.add(new Gene(x, y, 's'));
	}
	
	public static int getMaxGenes() {
		return maxGenes;
	}
	
	public void setChromosome(List<Gene> newChromosome) {
		chromosome = newChromosome;
	}
	
	public List<Gene> getChromosome() {
		return chromosome;
	}
	
	public void mixChromosome(int from, List<Gene> subChromosome) {
		chromosome.subList(from, chromosome.size()-1).clear();
		chromosome.addAll(subChromosome);
	}
	
	public List<Gene> getChromosomeRange(int from) {
		return chromosome.subList(from, chromosome.size()-1);
	}
	
	public boolean isGoal() {
		return goal;
	}

	public int getFitnes() {
		if (fitnes == -1) {
			fitnes = garden.getFitness();
			return fitnes;
		} else {
			return fitnes;
		}
	}

	public Garden getGarden() {
		return garden;
	}

	public int getGeneration() {
		return generation;
	}

	public void setGeneration(int generation) {
		this.generation = generation;
	}
	
	public int addGeneToChromosome(Gene newGene) {
		if (chromosome.size() < maxGenes) {
			chromosome.add(newGene);
			return chromosome.size();
		} else {
			return -1;
		}
	}
	
	public void mutateGeneInChromosome(int index, Gene newGene) {
		chromosome.set(index, newGene);
	}

	/**
	 * add new gene into exemplar representing starting position of Monk
	 * 
	 * @param x
	 *            - vertical position
	 * @param y
	 *            - horizontal position
	 * @return order of new gene
	 */
	public int addMonkStartingPosition(int x, int y, char direction) {
		if (chromosome.size() < maxGenes) {
			chromosome.add(new Gene(x, y, direction));
			return chromosome.size();
		} else {
			return -1;
		}
	}
	
	public boolean initializeGarden(LinkedList<Gene> stones) {
		this.stones = stones;
		if (chromosome.isEmpty()) {
			return false;
		} else {
			garden.initialize(chromosome, stones);
			fitnes = garden.getFitness();
			if (garden.isGoal()){
				goal=true;
			}
			return true;
		}
	}

}
