package logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Generator {

	/**
	 * @param x
	 *            - vertical length of garden
	 * @param y
	 *            - horizontal length of garden
	 * @param probability
	 *            - that current gene would be generated as starting position<br>
	 *            - should be greater than X+Y length
	 * @return chromosome - maximally half the size of garden perimeter
	 */
	public List<Gene> parentChromosomeInitialisation(int x, int y, int probability) {
		int count = x + y;
		List<Gene> output = new ArrayList<Gene>(count / 2);
		int i = 0;
		while (i < count) {
			i++;
			// 2 random values rendering - one for vertical one for horizontal
			// position
			Gene tmp = generateNewGene(x, y, probability);
			if (tmp!=null) {
				output.add(tmp);
			}
			
			if (tmp!=null) {
				System.out.println("gene " + i + " ("+ tmp.getXpos() + "," + tmp.getYpos() + "," + tmp.getDirection() + ")");
			}
		}
		return output;
	}
	
	private Gene generateNewGene(int x, int y, int probability) {
		int xpos;
		int ypos;
		Random rand = new Random();
		if (rand.nextInt() % 2 == 1) { // horizontal SP.
			if (rand.nextInt() % 2 == 1) { // up
				xpos = 0;
				ypos = rand.nextInt(probability);
				if (ypos < y) {
					return new Gene(xpos, ypos, 'd'); // char for direction of movement
				}
			} else { // down
				xpos = x - 1;
				ypos = rand.nextInt(probability);
				if (ypos < y) {
					return new Gene(xpos, ypos, 'u');
				}
			}
		} else { // vertical SP.
			if (rand.nextInt() % 2 == 1) { // left
				xpos = rand.nextInt(probability);
				ypos = 0;
				if (xpos < x) {
					return new Gene(xpos, ypos, 'r');
				}
			} else { // right
				xpos = rand.nextInt(probability);
				ypos = y - 1;
				if (xpos < x) {
					return new Gene(xpos, ypos, 'l');
				}
			}
		}
		return null;
	}
	/**
	 * Roulette technique of creating next generation
	 * @param parents
	 * @return mixed and mutated next generation from parents
	 */
	public List<Exemplar> roulette(List<Exemplar> parents) {
		ArrayList<Exemplar> roulette = new ArrayList<Exemplar>(Config.PARENTS*Config.X*Config.Y);
		ArrayList<Exemplar> sourceElements = new ArrayList<Exemplar>(Config.ROULETTE);
		int maxRand = 0;
		
		for (Exemplar p : parents) {
			maxRand += p.getFitnes();
			for (int i = 0; i < p.getFitnes(); i++) {
				roulette.add(p);
			}
		}
		Random rand = new Random();
		
		for (int i = 0; i < Config.ROULETTE; i++) {
			sourceElements.add(roulette.get(rand.nextInt(maxRand-1)));
		}
		
		return mixChromosomes(sourceElements);
	}
	private List<Exemplar> mixChromosomes(ArrayList<Exemplar> sourceElements) {
		List<Exemplar> mixed = new ArrayList<Exemplar>();
		Random rand = new Random();
		int p;
		for (int i = 0; i < sourceElements.size(); i++) {
			for (int j = i+1; j < sourceElements.size(); j++) {
				if (rand.nextInt(100) < Config.MIX_PROBABILITY) {
					p = sourceElements.get(i).getChromosome().size() < sourceElements.get(j).getChromosome().size() ? 
							rand.nextInt(sourceElements.get(i).getChromosome().size()-2)+1 : 
							rand.nextInt(sourceElements.get(j).getChromosome().size()-2)+1;
					List<Gene> subChromosome1 = new ArrayList<Gene>(sourceElements.get(i).getChromosomeRange(p));
					List<Gene> subChromosome2 = new ArrayList<Gene>(sourceElements.get(j).getChromosomeRange(p));
					mixed.add(new Exemplar(sourceElements.get(i)));
					mixed.get(mixed.size()-1).mixChromosome(p, subChromosome2);
					mixed.add(new Exemplar(sourceElements.get(j)));
					mixed.get(mixed.size()-1).mixChromosome(p, subChromosome1);
				} else {
					boolean canAdd= true;
					for (Exemplar e : mixed) {
						if (e == sourceElements.get(i)) {
							canAdd = false;
							break;
						}
					}
					if(canAdd) {
						mixed.add(sourceElements.get(i));
					}
				}
			}
		}
		return mutateChromosomes(mixed);
	}
	private List<Exemplar> mutateChromosomes(List<Exemplar> mixed) {
		Random random = new Random();
		
		// maximal count of mutated/created genes
		int count, p;
		for (Exemplar element : mixed) {
			if (Config.MUTATION_COUNT >= Exemplar.getMaxGenes()) {
				count = random.nextInt(Exemplar.getMaxGenes());
			} else {
				count = random.nextInt(Config.MUTATION_COUNT);
			}
			int changed[] = new int[count];
			for (int i = 0; i < count; i++) {
				p = random.nextInt(Exemplar.getMaxGenes());
				if (p>=element.getChromosome().size()) {
					p=element.getChromosome().size();
				}
				// checks if this gene wasn't mutated already
				for (int c : changed) {
					if (c==p) {
						p=-1;
						break;
					}
				}
				if (p==-1) {
					continue;
				}
				// if it is in range of his own genes, or i need to add new gene
				if (p < element.getChromosome().size()) {
					Gene tmp = generateNewGene(element.getGarden().getX(), element.getGarden().getY(), Config.PROBABILITY);
					if (tmp != null) {
						element.mutateGeneInChromosome(p, tmp);
					}
				} else {
					Gene tmp = generateNewGene(element.getGarden().getX(), element.getGarden().getY(), Config.PROBABILITY);
					if (tmp != null) {
						element.addGeneToChromosome(tmp);
					}
				}
			}
		}
		return mixed;
	}
}



