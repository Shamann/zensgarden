package logic;

import java.util.LinkedList;
import java.util.List;

/**
 * Class representing Zen's Garden. Contains field[][] and size of it. Methods to get string of field and fitnes 
 * @author Lukas
 *
 */
public class Garden {

	private byte field[][];
	private int x;
	private int y;
	/**
	 * Creates new garden
	 * @param sizeX - vertical
	 * @param sizeY - horizontal
	 */
	public Garden(int sizeX, int sizeY) {
		field = new byte[sizeX][sizeY];
		x=sizeX;
		y=sizeY;
	}
	
	public byte[][] getField() {
		return field;
	}
	
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	
	@Override
	public String toString() {
		String output = "";
		for(int i = 0; i<x; i++) {
			for (int j = 0; j<y; j++) {
				if (field[i][j]==-1) {
					output += " " + field[i][j];
				} else {
					output += field[i][j] < 10 ? "  " + field[i][j] : " " + field[i][j];
				}
			}
			output += "\n";
		}
		return output;
	}
	
	public void setField(byte[][] field) {
		this.field = field;
	}
	/**
	 * Checks that empty place is 0 and stone is -1, Monk path is 1..N. Other way returns wrong value.
	 * @return fitness value of garden
	 */
	public int getFitness(){
		int fitnes = 0;
		
		for(int i = 0; i<x; i++) {
			for (int j = 0; j<y; j++) {
				fitnes += field[i][j] > 0 ? 1 : 0;
			}
		}
		
		return fitnes;
	}

	public void initialize(List<Gene> chromosome, LinkedList<Gene> stones) {
		int order=0;
		char direction;
		for (Gene s : stones) {
			field[s.getXpos()][s.getYpos()] = -1;
		}
		for (Gene g : chromosome) {
			++order;
			direction = g.getDirection();
			int x=g.getXpos(), y=g.getYpos();
			boolean end = false;
			boolean endOfWay = false;
			while(!end && !endOfWay) {
				switch (direction) {
				case 'u' : while (!end && !endOfWay) {
						if (field[x][y]==0) {
							field[x][y] = (byte) order;
							if (x==0) {
								endOfWay = true;
								break;
							}
						} else {
							endOfWay=true;
							break;
						}
						if (x-1 >= 0 && field[x-1][y] == 0) {
							--x;
						} else {
							if (y-1>=0 && field[x][y-1]==0) {
								--y;
								direction = 'l';
								break;
							} else 
							if (y+1<=this.y-1 && field[x][y+1]==0) {
								++y;
								direction = 'r';
								break;
							} else {
								if (y==0 || y == this.y-1) {
									endOfWay = true;
								} else {
									end = true;
									field[x][y]=0;
								}
							}
						}
					} 
					break;
				case 'd' : while (!end && !endOfWay) {
						if (field[x][y]==0) {
							field[x][y] = (byte) order;
							if (x==this.x-1) {
								endOfWay = true;
								break;
							}
						} else {
							endOfWay=true;
							break;
						}
						if (x+1 <= this.x-1 && field[x+1][y] == 0) {
							++x;
						} else {
							if (y-1 >= 0 && field[x][y-1]==0) {
								--y;
								direction = 'l';
								break;
							} else 
							if (y+1 <= this.y-1 && field[x][y+1]==0) {
								++y;
								direction = 'r';
								break;
							} else {
								if (y==0 || y == this.y-1) {
									endOfWay = true;
								} else {
									end = true;
									field[x][y]=0;
								}
							}
						}
					}  
					break;
				case 'l' : while (!end && !endOfWay) {
						if (field[x][y]==0) {
							field[x][y] = (byte) order;
							if (y==0) {
								endOfWay = true;
								break;
							}
						} else {
							endOfWay=true;
							break;
						}
						if (y-1 >= 0 && field[x][y-1] == 0) {
							--y;
						} else {
							if (x-1 >= 0 && field[x-1][y]==0) {
								--x;
								direction = 'u';
								break;
							} else 
							if (x+1 <= this.x-1 && field[x+1][y]==0) {
								++x;
								direction = 'd';
								break;
							} else {
								if (x==0 || x == this.x-1) {
									endOfWay = true;
								} else {
									end = true;
									field[x][y]=0;
								}
							}
						}
					} 
					break;
				case 'r' : while (!end && !endOfWay) {
						if (field[x][y]==0) {
							field[x][y] = (byte) order;
							if (y==this.y-1) {
								endOfWay = true;
								break;
							}
						} else {
							endOfWay=true;
							break;
						}
						if (y+1 <= this.y-1 && field[x][y+1] == 0) {
							++y;
						} else {
							if (x-1 >= 0 && field[x-1][y]==0) {
								--x;
								direction = 'u';
								break;
							} else 
							if (x+1 <= this.x-1 && field[x+1][y]==0) {
								++x;
								direction = 'd';
								break;
							} else {
								if (x==0 || x == this.x-1) {
									endOfWay = true;
								} else {
									end = true;
									field[x][y]=0;
								}
							}
						}
					}  
					break;
				}
			}
			if (end) {
				return;
			}
		}
		
	}

	public boolean isGoal() {
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {
				if (field[i][j]==0) {
					return false;
				}
			}
		}
		return true;
	}
}
