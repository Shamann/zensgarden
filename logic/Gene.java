package logic;

public class Gene {

	private int xpos;
	private int ypos;
	private char direction;
	
	/**
	 * Gene of Monk starting point
	 * @param x - vertical coordinate
	 * @param y - horizontal coordinate
	 * @param direction - r,l,u,d char of stating movement direction
	 */
	public Gene(int x, int y,char direction) {
		xpos = x;
		ypos = y;
		this.direction = direction;
	}
	
	public String toString() {
		return "("+xpos+","+ypos+","+direction+")";
	}
	
	public int getXpos() {
		return xpos;
	}
	
	public int getYpos() {
		return ypos;
	}
	
	public char getDirection() {
		return direction;
	}
}
