package application;
	
import java.io.File;
import java.io.IOException;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;
import logic.Config;
import logic.Exemplar;
import logic.Generator;
import logic.Mapper;
import logic.Population;


public class GameStarter extends Application {
	
	@Override
	public void start(Stage primaryStage) {
		try {
			BorderPane root = (BorderPane)FXMLLoader.load(getClass().getResource("GamePane.fxml"));
			Scene scene = new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		consoleTester();
		launch(args);
	}
	
	private static void consoleTester() {
		// map reading and parsing
		Mapper map = new Mapper();
		File file = new File("files/zen5.in");
		try {
			map.readGarden(file);
			Config.X = map.getXlength();
			Config.Y = map.getYlength();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// parent generation generating
		Population elements = new Population();
		Generator generator = new Generator();
		
		// NUMBER OF STARTING PARENTS!
		// create first population of parents
		for (int a = 0; a < Config.PARENT_GENERATIONS; a++) {
			
			for (int i=0; i < Config.PARENTS; i++) {
				elements.parents.add(new Exemplar(map.getXlength(), map.getYlength(), 1));
			}
			// initialize gardens for parents
			for (Exemplar element : elements.parents) {
				element.setChromosome(generator.parentChromosomeInitialisation(map.getXlength(), map.getYlength(), Config.PROBABILITY));
				element.initializeGarden(map.getStones());
				System.out.println(element.getGarden().toString());
				if (element.isGoal()) {
					System.out.println("I Am Goal!");
					return;
				}
			}
			elements.nextGeneration = generator.roulette(elements.parents);
			
			// final cycle for generating generations and finding path with roulette mix & mutation
			for (int i=0; i < Config.MAX_GENERATIONS/Config.PARENT_GENERATIONS; i++) {
				for (Exemplar element : elements.nextGeneration) {
					element.initializeGarden(map.getStones());
//					System.out.println(element.toString());
//					System.out.println(element.getGarden().toString());
					if (element.isGoal()) {
						System.out.println("I Am Goal!");
						System.out.println(element.getGarden().toString());
						return;
					}
				}
				elements.nextGeneration = generator.roulette(elements.nextGeneration);
			}
		}
	}
	
}
